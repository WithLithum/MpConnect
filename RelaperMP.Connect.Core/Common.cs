﻿using System;

namespace RelaperMP.Connect.Core
{
    public static class Common
    {
        public static bool AskBooleanQuestion(string text)
        {
            Console.Write($"{text} (y/[n/other]): ");
            var read = Console.ReadKey();
            Console.WriteLine();
            return read.Key switch
            {
                ConsoleKey.Y => true,
                _ => false,
            };
        }

        public static string AskStringQuestion(string text)
        {
            Console.Write($"{text}: ");
            return Console.ReadLine();
        }

        public static string GetTime(DateTime ago)
        {
            var time = DateTime.Now - ago;

            if (time.TotalHours > 24)
            {
                return Math.Floor(time.TotalDays) + "天前";
            }
            if (time.TotalHours > 1)
            {
                return Math.Floor(time.TotalHours) + "小时前";
            }
            if (time.TotalMinutes > 1)
            {
                return Math.Floor(time.TotalMinutes) + "分钟前";
            }
            return Math.Floor(time.TotalSeconds) + "秒前";
        }
    }
}
