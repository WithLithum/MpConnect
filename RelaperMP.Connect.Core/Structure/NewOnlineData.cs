﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelaperMP.Connect.Core.Structure
{
    public class NewOnlineData
    {
        public bool AnyRoomOpen { get; set; }
        public string OpenRoom { get; set; }
        public string OpenRoomNumber { get; set; }
        public string NewsUrl { get; set; }
        public string SourceName { get; set; }
    }
}
