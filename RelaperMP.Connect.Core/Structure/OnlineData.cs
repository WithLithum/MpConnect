﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelaperMP.Connect.Core.Structure
{
    public class OnlineData
    {
        public bool Ess3Online { get; set; }
        public bool RelaperStackOnline { get; set; }
        public string Ess3No { get; set; }
        public string RelaperStackNo { get; set; }
        public DateTime Generated { get; set; }
    }
}
