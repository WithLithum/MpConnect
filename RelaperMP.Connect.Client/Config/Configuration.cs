﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RelaperMP.Connect.Client.Util;

namespace RelaperMP.Connect.Client.Config
{
    public class Configuration
    {
        public string CurrentSource { get; set; } = Url.DataJsonUrl;
    }
}
