﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RelaperMP.Connect.Client.Config;
using Newtonsoft.Json;

namespace RelaperMP.Connect.Client.Util
{
    public partial class ConfigFrm : Form
    {
        private Configuration _currentConfig;
        public ConfigFrm(Configuration config)
        {
            InitializeComponent();
            _currentConfig = config;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            _currentConfig.CurrentSource = this.textDataSource.Text;
            File.WriteAllText("Configuration.json", JsonConvert.SerializeObject(_currentConfig));

            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConfigFrm_Load(object sender, EventArgs e)
        {
            textDataSource.Text = _currentConfig.CurrentSource;
        }
    }
}
