﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RelaperMP.Connect.Client.Util
{
    internal static class TextUtil
    {
        internal static void PickRandomSongTitle(out string titleA, out string titleB)
        {
            var ranInt = new Random().Next(1, 6);
            switch (ranInt)
            {
                default:
                    titleA = MiscText.ResourceManager.GetString($"SongTitle{ranInt}A");
                    titleB = MiscText.ResourceManager.GetString($"SongTitle{ranInt}B");
                    break;
                case 1:
                    titleA = MiscText.SongTitle1A;
                    titleB = MiscText.SongTitle1B;
                    break;
                case 2:
                    titleA = MiscText.SongTitle2A;
                    titleB = MiscText.SongTitle2B;
                    break;
            }
        }
    }
}
