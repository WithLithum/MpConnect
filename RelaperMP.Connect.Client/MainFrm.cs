﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Web.WebView2.WinForms;
using Microsoft.Win32;
using Newtonsoft.Json;
using RelaperMP.Connect.Client.Config;
using RelaperMP.Connect.Client.Util;
using RelaperMP.Connect.Core;
using RelaperMP.Connect.Core.Structure;

namespace RelaperMP.Connect.Client
{
    public partial class MainFrm : Form
    {
        private NewOnlineData data;
        private Configuration _configuration;

        private readonly WebView2 _web = new WebView2()
        {
            Source = new Uri(Url.NewsUrl)
        };

        public MainFrm()
        {
            InitializeComponent();
            this.Controls.Add(_web);
            _web.Location = new Point(0, panel2.Height);
            _web.Height = (this.Height - panel1.Height) - panel2.Height - 55;
            _web.Width = this.Width - 20;
            _web.ContentLoading += _web_ContentLoading;
        }

        private void _web_ContentLoading(object sender, Microsoft.Web.WebView2.Core.CoreWebView2ContentLoadingEventArgs e)
        {
            // reserved
        }

        private void _web_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            buttonReloadPage.Enabled = false;
        }

        private void _web_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            buttonReloadPage.Enabled = true;
        }

        internal void RefreshData()
        {
            // 先取得信息（这里有可能报毒）
            var str = new WebClient().DownloadString(Url.DataJsonUrl);
            data = JsonConvert.DeserializeObject<NewOnlineData>(str);

            // 判断显示ESS3在线，还是离线
            if (data.AnyRoomOpen)
            {
                labelRoomOpen.Text = $"{data.OpenRoom} ({data.OpenRoomNumber})";
                labelRoomOpen.ForeColor = Color.LightGreen;
            }
            else
            {
                labelRoomOpen.Text = "无";
                labelRoomOpen.ForeColor = Color.Red;
            }

            _web.Source = new Uri(data.NewsUrl);

            // 判定上次更新
            labelLastUpdated.Text = $"来源：{data.SourceName}";
        }

        private void LoadConfig()
        {
            if (!File.Exists("Configuration.json"))
            {
                InitConfig();
            }
            else
            {
                _configuration = JsonConvert.DeserializeObject<Configuration>(File.ReadAllText("Configuration.json"));
            }

            try
            {
                _ = new Uri(_configuration.CurrentSource);
            }
            catch (UriFormatException)
            {
                MessageBox.Show("数据来源链接格式无效。重新创建配置文件。", "错误");
            }
        }

        private void InitConfig()
        {
            var config = new Configuration();
            this._configuration = config;
            File.WriteAllText("Configuration.json", JsonConvert.SerializeObject(config));
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            RefreshData();
            LoadConfig();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            // 误侦听，懒得改了
        }

        private void buttonReloadPage_Click(object sender, EventArgs e)
        {
            _web.Reload();
        }

        private void buttonAbout_Click(object sender, EventArgs e)
        {
            var dialogContent = new TaskDialogPage();
            dialogContent.Caption = "关于";
            dialogContent.Text = $"版本: {Assembly.GetExecutingAssembly().GetName().Version}\r\n操作系统: {Environment.OSVersion.VersionString}\r\n运行时版本: {Environment.Version}\r\n64位: 进度 {Environment.Is64BitProcess}，操作系统 {Environment.Is64BitOperatingSystem}\r\nGC内存数: {GC.GetTotalMemory(false) / 1024}";
            dialogContent.Heading = "RelaperMP.Connect";
            dialogContent.Icon = TaskDialogIcon.Information;
            dialogContent.Buttons.Add(TaskDialogButton.OK);
            TaskDialog.ShowDialog(dialogContent);
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            var path = (string)Registry.CurrentUser.OpenSubKey("SOFTWARE").OpenSubKey("Netease").OpenSubKey("MCLauncher").GetValue("InstallLocation");
            if (string.IsNullOrEmpty(path))
            {
                var dialogContent = new TaskDialogPage();
                dialogContent.Caption = "错误";
                dialogContent.Text = "未能找到我的世界启动器。";
                dialogContent.Heading = "启动失败";
                dialogContent.Icon = TaskDialogIcon.Error;
                dialogContent.Buttons.Add(TaskDialogButton.OK);
                TaskDialog.ShowDialog(dialogContent);
                return;
            }

            Process.Start(Path.Combine(path, "WPFLauncher.exe"));
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            var configfrm = new ConfigFrm(_configuration);
            if (configfrm.ShowDialog() == DialogResult.OK)
            {
                LoadConfig();
            }
        }

        private void buttonJoin_Click(object sender, EventArgs e)
        {
            var dialogContent = new TaskDialogPage();
            dialogContent.Caption = "帮助信息";
            dialogContent.Text = "1.点击“Start Game”按钮。\r\n2.登陆后点击“开始游戏”。\r\n3.点击“本地联机”。\r\n4.点击“推荐房间”后的“更多”按钮。\r\n5.搜索框内输入房间号。";
            dialogContent.Heading = "如何加入";
            dialogContent.Icon = TaskDialogIcon.Information;
            dialogContent.Buttons.Add(TaskDialogButton.OK);
            TaskDialog.ShowDialog(dialogContent);
        }
    }
}
