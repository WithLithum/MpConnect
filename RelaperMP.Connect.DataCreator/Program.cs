﻿using System;
using System.IO;
using Newtonsoft.Json;
using RelaperMP.Connect.Core;
using RelaperMP.Connect.Core.Structure;

namespace RelaperMP.Connect.DataCreator
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Data Generator, 2021");

            var essOnline = Common.AskBooleanQuestion("Display ESS3 online?");
            var stackOnline = Common.AskBooleanQuestion("Display Stack online?");

            var essNumber = string.Empty;
            var stackNumber = string.Empty;

            if (essOnline) essNumber = Common.AskStringQuestion("Ess3 Number?");
            if (stackOnline) stackNumber = Common.AskStringQuestion("Stack Number?");

            var data = new OnlineData()
            {
                Ess3Online = essOnline,
                RelaperStackOnline = stackOnline,
                Generated = DateTime.Now,
                Ess3No = essNumber,
                RelaperStackNo = stackNumber
            };

            File.WriteAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "srv.json"), JsonConvert.SerializeObject(data));
            Console.WriteLine("Data generation success!");
        }
    }
}
